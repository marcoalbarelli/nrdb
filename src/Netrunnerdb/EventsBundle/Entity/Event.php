<?php

namespace Netrunnerdb\EventsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 */
class Event
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $championship;

    /**
     * @var \DateTime
     */
    private $datetimestart;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $locality;

    /**
     * @var string
     */
    private $area;

    /**
     * @var string
     */
    private $country;

    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $longitude;

    /**
     * @var integer
     */
    private $nbplayers;

    /**
     * @var boolean
     */
    private $past;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set championship
     *
     * @param integer $championship
     * @return Event
     */
    public function setChampionship($championship)
    {
        $this->championship = $championship;

        return $this;
    }

    /**
     * Get championship
     *
     * @return integer
     */
    public function getChampionship()
    {
        return $this->championship;
    }

    /**
     * Set datetimestart
     *
     * @param \DateTime $datetimestart
     * @return Event
     */
    public function setDatetimestart($datetimestart)
    {
    	// converting datetime to UTC because MySQL doesn't save the timezone information in datetime columns
    	// so we need the datetime without any timezone modifier
    	$datetimestart->setTimezone(new \DateTimeZone('UTC'));
    	
        $this->datetimestart = $datetimestart;

        return $this;
    }

    /**
     * Get datetimestart
     *
     * @return \DateTime
     */
    public function getDatetimestart()
    {
    	// when converting to php DateTime, mysql assumes the timezone is the one of the server
    	// so we have to re-write the timezone information to 'UTC' 
    	$datetimestart = \DateTime::createFromFormat(
    			'Y-m-d H:i:s',
    			$this->datetimestart->format('Y-m-d H:i:s'),
    			new \DateTimeZone('UTC'));
    	
    	return $datetimestart;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return Event
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Event
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Event
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set locality
     *
     * @param string $locality
     * @return Event
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality
     *
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set area
     *
     * @param string $area
     * @return Event
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Event
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Event
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Event
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set nbplayers
     *
     * @param integer $nbplayers
     * @return Event
     */
    public function setNbplayers($nbplayers)
    {
        $this->nbplayers = $nbplayers;

        return $this;
    }

    /**
     * Get nbplayers
     *
     * @return integer
     */
    public function getNbplayers()
    {
        return $this->nbplayers;
    }

    /**
     * Set past
     *
     * @param boolean $past
     * @return Event
     */
    public function setPast($past)
    {
        $this->past = $past;

        return $this;
    }

    /**
     * Get past
     *
     * @return boolean
     */
    public function getPast()
    {
        return $this->past;
    }
    /**
     * @var \Netrunnerdb\UserBundle\Entity\User
     */
    private $user;

    /**
     * @var \Netrunnerdb\CardsBundle\Entity\Pack
     */
    private $lastPack;

    /**
     * @var \Netrunnerdb\EventsBundle\Entity\Tier
     */
    private $tier;


    /**
     * Set user
     *
     * @param \Netrunnerdb\UserBundle\Entity\User $user
     * @return Event
     */
    public function setUser(\Netrunnerdb\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Netrunnerdb\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set lastPack
     *
     * @param \Netrunnerdb\CardsBundle\Entity\Pack $lastPack
     * @return Event
     */
    public function setLastPack(\Netrunnerdb\CardsBundle\Entity\Pack $lastPack = null)
    {
        $this->lastPack = $lastPack;

        return $this;
    }

    /**
     * Get lastPack
     *
     * @return \Netrunnerdb\CardsBundle\Entity\Pack
     */
    public function getLastPack()
    {
        return $this->lastPack;
    }

    /**
     * Set tier
     *
     * @param \Netrunnerdb\EventsBundle\Entity\Tier $tier
     * @return Event
     */
    public function setTier(\Netrunnerdb\EventsBundle\Entity\Tier $tier = null)
    {
        $this->tier = $tier;

        return $this;
    }

    /**
     * Get tier
     *
     * @return \Netrunnerdb\EventsBundle\Entity\Tier
     */
    public function getTier()
    {
        return $this->tier;
    }
    /**
     * @var string
     */
    private $country_code;


    /**
     * Set country_code
     *
     * @param string $countryCode
     * @return Event
     */
    public function setCountryCode($countryCode)
    {
        $this->country_code = $countryCode;

        return $this;
    }

    /**
     * Get country_code
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->country_code;
    }
    
    public function getPrettyName()
    {
        return preg_replace('/[^a-z0-9]+/', '-', mb_strtolower($this->name));
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $attendances;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attendances = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add attendances
     *
     * @param \Netrunnerdb\EventsBundle\Entity\Attendance $attendances
     * @return Event
     */
    public function addAttendance(\Netrunnerdb\EventsBundle\Entity\Attendance $attendances)
    {
        $this->attendances[] = $attendances;

        return $this;
    }

    /**
     * Remove attendances
     *
     * @param \Netrunnerdb\EventsBundle\Entity\Attendance $attendances
     */
    public function removeAttendance(\Netrunnerdb\EventsBundle\Entity\Attendance $attendances)
    {
        $this->attendances->removeElement($attendances);
    }

    /**
     * Get attendances
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttendances()
    {
        return $this->attendances;
    }
}
