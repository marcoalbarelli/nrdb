<?php
namespace Netrunnerdb\EventsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Netrunnerdb\EventsBundle\Entity\Event;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Netrunnerdb\EventsBundle\Entity\Attendance;

class EventController extends Controller
{
    public function listFutureAction(Request $request)
    {
        $response = new Response();
        $response->setPublic();
        $response->setMaxAge($this->container->getParameter('short_cache'));
        
        $em = $this->getDoctrine()->getManager();
        
        $list_events = $em->getRepository('NetrunnerdbEventsBundle:Event')->findBy(["past" => FALSE], ["datetimestart" => "DESC"]);

        $now = new \DateTime();
        $events = [];
        $locations = [];
        
        /* @var $event Event */
        foreach($list_events as $event) {
            if($event->getDatetimestart() < $now) {
                $event->setPast(TRUE);
            } else {
                $events[] = $event;
                $locations[] = [ $event->getName(), $event->getLatitude(), $event->getLongitude() ];
            }
        }
        $em->flush();
        
        return $this->render('NetrunnerdbEventsBundle:Event:event_list_future.html.twig', array(
                'pagetitle' => 'Incoming Events',
                'events' => $events,
                'locations' => $locations
        ));
    }
    
    public function listPastAction($page = 1, Request $request)
    {
        $response = new Response();
        $response->setPublic();
        $response->setMaxAge($this->container->getParameter('short_cache'));
        
        $limit = 30;
        if ($page < 1)
            $page = 1;
        $start = ($page - 1) * $limit;
        
        $em = $this->getDoctrine()->getManager();
    
        $events = $em->getRepository('NetrunnerdbEventsBundle:Event')->findBy(["past" => TRUE], ["datetimestart" => "DESC"]);

        $maxcount = count($events);
        $events = array_slice($events, $start, $limit);
        
        // pagination : calcul de nbpages // currpage // prevpage // nextpage
        // à partir de $start, $limit, $count, $maxcount, $page
        
        $currpage = $page;
        $prevpage = max(1, $currpage - 1);
        $nbpages = min(10, ceil($maxcount / $limit));
        $nextpage = min($nbpages, $currpage + 1);
        
        $route = $request->get('_route');
        
        $params = $request->query->all();
        
        $pages = array();
        for ($page = 1; $page <= $nbpages; $page ++) {
            $pages[] = array(
                    "numero" => $page,
                    "url" => $this->generateUrl($route, $params + array(
                            "page" => $page
                    )),
                    "current" => $page == $currpage
            );
        }
        
        return $this->render('NetrunnerdbEventsBundle:Event:event_list_past.html.twig', array(
                'pagetitle' => 'Past Events',
                'events' => $events,
                'route' => $route,
                'pages' => $pages,
                'prevurl' => $currpage == 1 ? null : $this->generateUrl($route, $params + array(
                        "page" => $prevpage
                )),
                'nexturl' => $currpage == $nbpages ? null : $this->generateUrl($route, $params + array(
                        "page" => $nextpage
                ))
        ));
    }
    
    public function listDateAction($date, Request $request)
    {
    
    }
    
    public function listCountryAction($country, Request $request)
    {
    
    }
    
    public function createAction(Request $request)
    {
        

        $list_packs = $this->getDoctrine()->getRepository('NetrunnerdbCardsBundle:Pack')->findBy(array(), array("released" => "DESC", "number" => "DESC"));
        $packs = array();
        /* @var $pack \Netrunnerdb\CardsBundle\Entity\Pack */
        foreach($list_packs as $pack) {
            if($pack->getCycle()->getNumber() === 0) continue;
            $packs[] = array(
                    "name" => $pack->getName($this->getRequest()->getLocale()),
                    "code" => $pack->getCode(),
            );
        }
        
        $list_tiers = $this->getDoctrine()->getRepository('NetrunnerdbEventsBundle:Tier')->findBy(array(), array("name" => "ASC"));
        $tiers = array();
        /* @var $tier \Netrunnerdb\EventsBundle\Entity\Tier */
        foreach($list_tiers as $tier) {
            $tiers[] = array(
                    "name" => $tier->getName(),
                    "id" => $tier->getId(),
            );
        }
        
        $timezones = \DateTimeZone::listIdentifiers();
        
        return $this->render('NetrunnerdbEventsBundle:Event:event_create.html.twig', array(
                'packs' => $packs,
                'tiers' => $tiers,
                'timezones' => $timezones
        ));
    }
    
    public function saveAction(Request $request)
    {
        $user = $this->getUser();
        
        $em = $this->getDoctrine()->getManager();
        
        $id = $request->request->get('id');
        if(isset($id)) {
            $event = $em->getRepository('NetrunnerdbEventsBundle:Event')->find($id);
            if(!$event) {
                throw new BadRequestHttpException("Unable to find event.");
            }
            if($event->getUser()->getId() !== $user->getId()) {
                throw UnauthorizedHttpException("You cannot manage this event.");
            }
        } else {
            $event = new Event();
        }
        
        $address = $request->request->get('address');
        $event->setAddress($address);
        
        $area = $request->request->get('area');
        $event->setArea($area);
        
        $championship = intval($request->request->get('championship'));
        $event->setChampionship($championship);
        
        $country = $request->request->get('country');
        $event->setCountry($country);
        
        $countryCode = $request->request->get('country_code');
        $event->setCountryCode($countryCode);

        $timezone = $request->request->get('timezone');
        $event->setTimezone($timezone);
        
        $datetimestart = new \DateTime($request->request->get('datetimestart'));
        $event->setDatetimestart($datetimestart);
        
        $description = $request->request->get('description');
        $event->setDescription($description);
        
        $lastPack = $em->getRepository('NetrunnerdbCardsBundle:Pack')->findOneBy(array('code' => $request->request->get('last_pack')));
        $event->setLastPack($lastPack);
        
        $latitude = floatval($request->request->get('latitude'));
        $event->setLatitude($latitude);
        
        $locality = $request->request->get('locality');
        $event->setLocality($locality);
        
        $location = $request->request->get('location');
        $event->setLocation($location);
        
        $longitude = floatval($request->request->get('longitude'));
        $event->setLongitude($longitude);
        
        $name = $request->request->get('name');
        $event->setName($name);
        
        $event->setNbplayers(0);
        
        $now = new \DateTime();
        $event->setPast($datetimestart < $now);
        
        $tier = $em->getRepository('NetrunnerdbEventsBundle:Tier')->find(intval($request->request->get('tier')));
        $event->setTier($tier);
        
        $user = $this->getUser();
        $event->setUser($user);
        
        $em->persist($event);
        $em->flush();
        
        return $this->redirect($this->generateUrl('event_view', array('_locale' => $request->getLocale(), 'event_id' => $event->getId(), 'event_name' => $event->getPrettyName())));
    }
    
    public function viewAction($event_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('NetrunnerdbEventsBundle:Event')->find($event_id);
        
        // events at the same time +/- 2 days
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $interval = new \DateInterval('P2D');
        $criteria->where($criteria->expr()->gt('datetimestart', $event->getDatetimestart()->sub($interval)));
        $criteria->andWhere($criteria->expr()->lt('datetimestart', $event->getDatetimestart()->add($interval)));
        $criteria->andWhere($criteria->expr()->neq('id', $event->getId()));
        $events = $em->getRepository('NetrunnerdbEventsBundle:Event')->matching($criteria);
        
        $type_identity = $em->getRepository('NetrunnerdbCardsBundle:Type')->findBy(array('name' => "Identity"));
        $side_corp = $em->getRepository('NetrunnerdbCardsBundle:Side')->findOneBy(array('name' => "Corp"));
        $side_runner = $em->getRepository('NetrunnerdbCardsBundle:Side')->findOneBy(array('name' => "Runner"));
        $corps = $em->getRepository('NetrunnerdbCardsBundle:Card')->findBy(array('type' => $type_identity, 'side' => $side_corp), array('code' => 'ASC'));
        $runners = $em->getRepository('NetrunnerdbCardsBundle:Card')->findBy(array('type' => $type_identity, 'side' => $side_runner), array('code' => 'ASC'));
        
        return $this->render('NetrunnerdbEventsBundle:Event:event_view.html.twig', array(
                'pagetitle' => $event->getName(),
                'event' => $event,
        		'events' => $events,
                'corps' => $corps,
                'runners' => $runners
        ));
    }
    
    public function manageAction($event_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('NetrunnerdbEventsBundle:Event')->find($event_id);
        if(!$event) {
            throw new NotFoundHttpException("Unable to find event.");
        }
        
        $list_packs = $em->getRepository('NetrunnerdbCardsBundle:Pack')->findBy(array(), array("released" => "DESC", "number" => "DESC"));
        $packs = array();
        /* @var $pack \Netrunnerdb\CardsBundle\Entity\Pack */
        foreach($list_packs as $pack) {
            if($pack->getCycle()->getNumber() === 0) continue;
            $packs[] = array(
                    "name" => $pack->getName($this->getRequest()->getLocale()),
                    "code" => $pack->getCode(),
            );
        }
        
        $list_tiers = $em->getRepository('NetrunnerdbEventsBundle:Tier')->findBy(array(), array("name" => "ASC"));
        $tiers = array();
        /* @var $tier \Netrunnerdb\EventsBundle\Entity\Tier */
        foreach($list_tiers as $tier) {
            $tiers[] = array(
                    "name" => $tier->getName(),
                    "id" => $tier->getId(),
            );
        }
        
        $timezones = \DateTimeZone::listIdentifiers();
        
        $type_identity = $em->getRepository('NetrunnerdbCardsBundle:Type')->findBy(array('name' => "Identity"));
        $side_corp = $em->getRepository('NetrunnerdbCardsBundle:Side')->findOneBy(array('name' => "Corp"));
        $side_runner = $em->getRepository('NetrunnerdbCardsBundle:Side')->findOneBy(array('name' => "Runner"));
        $corps = $em->getRepository('NetrunnerdbCardsBundle:Card')->findBy(array('type' => $type_identity, 'side' => $side_corp), array('code' => 'ASC'));
        $runners = $em->getRepository('NetrunnerdbCardsBundle:Card')->findBy(array('type' => $type_identity, 'side' => $side_runner), array('code' => 'ASC'));
        
        return $this->render('NetrunnerdbEventsBundle:Event:event_manage.html.twig', array(
                'pagetitle' => "Manage your event",
                'event' => $event,
                'packs' => $packs,
                'tiers' => $tiers,
                'timezones' => $timezones,
                'corps' => $corps,
                'runners' => $runners
                
        ));
    }
    
    public function commentAction($event_id, Request $request)
    {
    
    }

    public function uploadNrtmAction($event_id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $cards_data \Netrunnerdb\CardsBundle\Services\CardsData */
        $cards_data = $this->get('cards_data');
        
        $event = $em->getRepository('NetrunnerdbEventsBundle:Event')->find($event_id);
        if(!$event) {
            throw new NotFoundHttpException("Unable to find event.");
        }
        $attendanceRepo = $em->getRepository('NetrunnerdbEventsBundle:Attendance');
        
        $uploadedFile = $request->files->get('upfile');
        if (! isset($uploadedFile)) {
            throw new BadRequestHttpException("No file");
        }
        $filename = $uploadedFile->getPathname();
        
        if (function_exists("finfo_open")) {
            // return mime type ala mimetype extension
            $finfo = finfo_open(FILEINFO_MIME);
        
            // check to see if the mime-type starts with 'text'
            $is_text = substr(finfo_file($finfo, $filename), 0, 4) == 'text' || substr(finfo_file($finfo, $filename), 0, 15) == "application/xml";
            if (! $is_text) {
                throw new BadRequestHttpException("Bad file");
            }
        }
        
        $json = json_decode(file_get_contents($filename), TRUE);
        
        // verify NRTM version
        $nrtm_version = explode('.', $json['version']);
        if($nrtm_version[0] === '1' && $nrtm_version[2] < 17) {
            $this->get('session')->getFlashBag()->set('error', "NRTM version 1.17 is required. Sorry.");
            return $this->redirect($this->generateUrl('event_manage', array('_locale' => $request->getLocale(), 'event_id' => $event->getId())));
        }
        
        // name of tourney
        $name = $json['name'];
        if(!empty($name)) {
            $event->setName($name);
        }
        
        $players = $json['players'];
        foreach($players as $player) {
            // find player name
            // in the form: Firstname Lastname (Surname)
            $playername = $player['name'];
            $matches = array();
            if(preg_match('/\((.+)\)/', $playername, $matches)) {
                $playername = $matches[1];
            }
            
            // unvalidate all attendance
            $attendances = $event->getAttendances();
            foreach($attendances as $attendance) {
                $attendance->setValidated(FALSE);
            }
            
            // find attendance
            $attendance = $attendanceRepo->findOneBy(array('playername' => $playername, 'event' => $event));
            if(!$attendance) {
                $attendance = new Attendance();
                $attendance->setPlayername($playername);
                $attendance->setEvent($event);
                $attendance->setTs(new \DateTime);
                $em->persist($attendance);
                
                $event->setNbplayers($event->getNbplayers() + 1);
            }
            
            // find identities
            $corpIdentity = $cards_data->find_identity($player['corpIdentity']);
            $attendance->setCorpIdentity($corpIdentity);
            
            $runnerIdentity = $cards_data->find_identity($player['runnerIdentity']);
            $attendance->setRunnerIdentity($runnerIdentity);
            
            // update attendance
            $attendance->setIsBye($player['isBye']);
            $attendance->setIsDisqualified($player['disqualified']);
            $attendance->setIsForfeit($player['forfeit']);
            $attendance->setIsValidated(TRUE);
            
            $attendance->setPrestige($player['prestige']);
            $attendance->setSos($player['strengthOfSchedule']);
            $attendance->setRank($player['rank']);
        }

        // check date of event
        $now = new \DateTime;
        $now->setTime($now->format('G'), 0, 0);
        if($event->getDatetimestart() > $now) {
            $event->setDatetimestart($now);
        }
        $event->setPast(TRUE);
        
        // all good
        $em->flush();
        
        return $this->redirect($this->generateUrl('event_view', array('_locale' => $request->getLocale(), 'event_id' => $event->getId(), 'event_name' => $event->getPrettyName())));
    }
}
